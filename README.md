# unity-iris-vvvv #

This is a brief definition how a proof-of-concept interconnection between a game-engine like unity, the iris framework and vvvv should work.

The goal is to use unity as rendering engine with all its good asset workflow but use vvvv for frame-sync video playback and as final output renderer. on top of that the iris framework should be used to control parameters either in vvvv as well as in unity.

Therefore the unity side need to send its final color- and depthbuffer to vvvv as well as view- and projectionmatrices from final camera. VVVV will receive that data so is able to recompose scene and generate 3d aware content.

Further Unity needs to implement iris client to talk to iris cluster, exposing, reading and writing parameters. 

For now we would use a fixed resolution of 1920x1080px. But that should be flexible in future.

What is need to be done for a first proof of concept:

### unity ###

* get color buffer as R8G8B8A8 from final renderer with [unity api](https://docs.unity3d.com/560/Documentation/ScriptReference/RenderTexture-colorBuffer.html)
* get depth buffer as R16F from final renderer with  [unity api](https://docs.unity3d.com/560/Documentation/ScriptReference/RenderTexture-depthBuffer.html)
* get view- and projectionmatrices with [unity api](https://docs.unity3d.com/560/Documentation/ScriptReference/Camera-projectionMatrix.html)
* create directx11 shared resource with twice of rendering dimension to combine buffers and use only one shared resource for now (in this case that would be 3840x1080px@R8G8B8A8)
* write color buffer to the first half of shared resource
* convert depthbuffer R16F to R8G8 and write to second half of shared resource 
* to keep camera in sync with content we convert view- and projection matrices to B8A8 components and write to second half of shared resource (which would need 32x32px in Blue and Alpha channel, so still some space left)
* share shared resource pointer using write pin - read below

### unity-iris ###
* create slim api to access values (r/w) from inside unity project (for a first step we would just use double)
* generate uuids for values which then need to be saved alongside unity project - may this be a property of value like a tag-field in v4
* to cover iris communication standard the subtypes of v4 io-box value need to be generated
* implement basic iris client which communicate with front-end service
* to set values in vvvv from unity use iris "write-pin" feature (beware that this will not be in sync with video stream 
* receive iris-cluster masterclock and bind unity timeline to it

### vvvv ####
* receive pointer for shared resource
* grab first half of shared resource for R8G8B8A8 Color Buffer
* recompose R16F DepthBuffer from second half R8G8 of shared resource
* readback B8A8 pixels from second half and recompose view and projection matrix
* Compose Scene and Render
* to set values in unity from vvvv use iris "write-pin" feature

### open questions ###
* what about latency (as readback and recompose of viewproj matrices in vvvv will result in at least one frame delay between camera and texture)
* would it be possible to control place-holder values with unity timeline - aka write-pin instances (if so one could think about controlling vvvv parameters with unitys timeline)
* what about sync issues between unity and vvvv renderer
* is it possible to bind unity and vvvv to separate devices (one could think of second gpu)
* possibility of sharing resource between devices
* possibility of mixing nvida and synced ati gpu in one system (this should be solved with video-ip workflow)
* any reasons to use depthbuffer with more than 16Bit?

### further thoughts ###
* as shared resource works at local client only texture transmission should work with a uncompressed video-ip standard in future 
* development of a sophisticated unity client for the upcoming iris-version may be event-based
* think about R32F Depthbuffer conversion to B8A8 components (which would result it doubled array size)
* heading to 4k/60 
* integrating fcp into iris to control vvvv and unity with same timeline
* think about how that could work within a unity-rendering cluster as well as multiple vvvv clients